export const SWITCH_CONTROL_KEYBOARD = "switchControlKeyboard";
export const SHOW_HINTS = "showHints";
export const CHANGE_VOLUME = "changeVolume";
export const SET_ACTIVE_NOTE = "setActiveNote";
export const REMOVE_ACTIVE_NOTE = "removeActiveNote";
export const SWITCH_NUM_OF_OCTAVES = "switchNumOfOctaves";
export const SWITCH_OCTAVE = "switchOctave";
