import { LOW } from "../constants/index";
import { createStoreon } from "storeon";
import {
  SWITCH_CONTROL_KEYBOARD,
  SWITCH_NUM_OF_OCTAVES,
  SHOW_HINTS,
  CHANGE_VOLUME,
  SET_ACTIVE_NOTE,
  REMOVE_ACTIVE_NOTE,
  SWITCH_OCTAVE,
} from "./events";

const appStore = (store) => {
  store.on("@init", () => ({
    hintsShown: false,
    useKeyboard: false,
    volume: 0.5,
    activeNotes: [],
    numOfOctaves: 2,
    octave: LOW,
  }));
  store.on(SWITCH_CONTROL_KEYBOARD, (state, shouldSwitch) => ({
    ...state,
    useKeyboard: shouldSwitch,
  }));
  store.on(SHOW_HINTS, (state, shouldShowHints) => ({
    ...state,
    hintsShown: shouldShowHints,
  }));
  store.on(CHANGE_VOLUME, (state, volumeVal) => ({
    ...state,
    volume: volumeVal,
  }));
  store.on(SET_ACTIVE_NOTE, (state, note) => ({
    ...state,
    activeNotes: [...state.activeNotes, note],
  }));
  store.on(REMOVE_ACTIVE_NOTE, (state, note) => ({
    ...state,
    activeNotes: state.activeNotes.filter((item) => item !== note),
  }));
  store.on(SWITCH_NUM_OF_OCTAVES, (state, num) => ({
    ...state,
    numOfOctaves: num,
  }));
  store.on(SWITCH_OCTAVE, (state, oct) => ({ ...state, octave: oct }));
};

const store = createStoreon([appStore]);

export default store;
