import {
  NOTES,
  HIGHER_OCTAVE_NOTES,
  SEMICOLON,
  HIGH,
  LOW,
  KEY_NAMES,
} from "../constants/index";

export const isSharp = (note) => note.noteName[2] === "s";

export const getNotes = (useKeyboard, numOfOctaves, octave) => {
  if (numOfOctaves === 1) {
    return octave === LOW ? NOTES.slice(0, 12) : HIGHER_OCTAVE_NOTES;
  }
  return useKeyboard ? NOTES.slice(0, 17) : NOTES;
};

export const getNoteByKey = (key, octave) => {
  if (octave === HIGH) {
    return HIGHER_OCTAVE_NOTES.find((note) => note.keyName === key);
  }
  return NOTES.find((note) => note.keyName === key);
};

export const getKeyLetterFromKey = (keyName) => {
  if (keyName === SEMICOLON) return ";";
  return keyName[3];
};

export const prevent = (e) => e.preventDefault();

export const writeNoteForHints = (note) => {
  let wnote = note.noteName.replace("s", "#");
  return wnote.length === 3 ? wnote[0] + wnote[2] : wnote[0];
};

export function delay(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms);
  });
}

export const isKeyWithNote = (key) => KEY_NAMES.includes(key);

export const thereAreActiveNotes = (activeNotes) => activeNotes.length > 0;
