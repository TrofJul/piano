import React from "react";
import styles from "./App.module.css";
import "./components/KeyBoard/KeyBoard";
import KeyBoard from "./components/KeyBoard/KeyBoard";
import PageHeader from "./components/PageHeader/PageHeader";
import Controls from "./components/Controls/Controls";
import { StoreContext } from "storeon/react";
import store from "./store/index";

class App extends React.Component {
  render() {
    return (
      <StoreContext.Provider value={store}>
        <div className={styles.App}>
          <PageHeader />
          <main>
            <Controls />
            <KeyBoard />
          </main>
        </div>
      </StoreContext.Provider>
    );
  }
}

export default App;
