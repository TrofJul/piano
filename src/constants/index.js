export const NOTE_NAMES = [
  "C2",
  "C2s",
  "D2",
  "D2s",
  "E2",
  "F2",
  "F2s",
  "G2",
  "G2s",
  "A3",
  "A3s",
  "B3",
  "C3",
  "C3s",
  "D3",
  "D3s",
  "E3",
  "F3",
  "F3s",
  "G3",
  "G3s",
  "A4",
  "A4s",
  "B4",
];

export const KEY_NAMES = [
  "KeyA",
  "KeyW",
  "KeyS",
  "KeyE",
  "KeyD",
  "KeyF",
  "KeyT",
  "KeyG",
  "KeyY",
  "KeyH",
  "KeyU",
  "KeyJ",
  "KeyK",
  "KeyO",
  "KeyL",
  "KeyP",
  "Semicolon",
];

export const NOTES = [
  {
    noteName: "C2",
    keyName: "KeyA",
  },
  {
    noteName: "C2s",
    keyName: "KeyW",
  },
  {
    noteName: "D2",
    keyName: "KeyS",
  },
  {
    noteName: "D2s",
    keyName: "KeyE",
  },
  {
    noteName: "E2",
    keyName: "KeyD",
  },
  {
    noteName: "F2",
    keyName: "KeyF",
  },
  {
    noteName: "F2s",
    keyName: "KeyT",
  },
  {
    noteName: "G2",
    keyName: "KeyG",
  },
  {
    noteName: "G2s",
    keyName: "KeyY",
  },
  {
    noteName: "A3",
    keyName: "KeyH",
  },
  {
    noteName: "A3s",
    keyName: "KeyU",
  },
  {
    noteName: "B3",
    keyName: "KeyJ",
  },
  {
    noteName: "C3",
    keyName: "KeyK",
  },
  {
    noteName: "C3s",
    keyName: "KeyO",
  },
  {
    noteName: "D3",
    keyName: "KeyL",
  },
  {
    noteName: "D3s",
    keyName: "KeyP",
  },
  {
    noteName: "E3",
    keyName: "Semicolon",
  },
  {
    noteName: "F3",
    keyName: null,
  },
  {
    noteName: "F3s",
    keyName: null,
  },
  {
    noteName: "G3",
    keyName: null,
  },
  {
    noteName: "G3s",
    keyName: null,
  },
  {
    noteName: "A4",
    keyName: null,
  },
  {
    noteName: "A4s",
    keyName: null,
  },
  {
    noteName: "B4",
    keyName: null,
  },
];

export const HIGHER_OCTAVE_NOTES = [
  {
    noteName: "C3",
    keyName: "KeyA",
  },
  {
    noteName: "C3s",
    keyName: "KeyW",
  },
  {
    noteName: "D3",
    keyName: "KeyS",
  },
  {
    noteName: "D3s",
    keyName: "KeyE",
  },
  {
    noteName: "E3",
    keyName: "KeyD",
  },
  {
    noteName: "F3",
    keyName: "KeyF",
  },
  {
    noteName: "F3s",
    keyName: "KeyT",
  },
  {
    noteName: "G3",
    keyName: "KeyG",
  },
  {
    noteName: "G3s",
    keyName: "KeyY",
  },
  {
    noteName: "A4",
    keyName: "KeyH",
  },
  {
    noteName: "A4s",
    keyName: "KeyU",
  },
  {
    noteName: "B4",
    keyName: "KeyJ",
  },
];

export const SEMICOLON = "Semicolon";
export const LOW = "low";
export const HIGH = "high";

export const MELODY = [
  { Key: "KeyS", time: 500 },
  { Key: "KeyD", time: 300 },
  { Key: "KeyT", time: 300 },
  { Key: "KeyS", time: 300 },
  { Key: "KeyW", time: 700 },
  { Key: "KeyS", time: 400 },
  { Key: "KeyD", time: 300 },
  { Key: "KeyT", time: 300 },
  { Key: "KeyT", time: 800 },
  { Key: "KeyG", time: 300 },
  { Key: "KeyH", time: 300 },
  { Key: "KeyJ", time: 300 },
  { Key: "KeyD", time: 800 },
  { Key: "KeyF", time: 700 },
  { Key: "KeyT", time: 300 },
  { Key: "KeyG", time: 300 },
  { Key: "KeyH", time: 300 },
  { Key: "KeyL", time: 600 },
  { Key: "KeyO", time: 300 },
  { Key: "KeyJ", time: 300 },
  { Key: "KeyT", time: 300 },
  { Key: "KeyH", time: 600 },
  { Key: "KeyY", time: 400 },
  { Key: "KeyG", time: 500 },
];
