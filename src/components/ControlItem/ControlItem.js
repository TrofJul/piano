import React from "react";
import styles from "./ControlItem.module.css";

function ControlItem(props) {
  return (
    <div className={styles.item}>
      {props.title && <span className={styles.option}>{props.title}</span>}
      {props.leftOption && (
        <span className={styles.option}>{props.leftOption}</span>
      )}
      <div className={styles.button}>
        <input
          type="checkbox"
          className={styles.checkbox}
          onClick={props.onClick}
        />
        <div className={styles.knobs}></div>
        <div className={styles.layer}></div>
      </div>
      {props.rightOption && (
        <span className={styles.option}>{props.rightOption}</span>
      )}
    </div>
  );
}

export default ControlItem;
