import React from "react";
import styles from "./KeyBoard.module.css";
import Keys from "../Keys/Keys";
import { connectStoreon } from "storeon/react";
import { getNoteByKey, isKeyWithNote } from "../../utils/index";
import { REMOVE_ACTIVE_NOTE, SET_ACTIVE_NOTE } from "../../store/events";

class KeyBoard extends React.PureComponent {
  componentDidUpdate() {
    if (this.props.useKeyboard) {
      document.addEventListener("keydown", this.keyDown);
      document.addEventListener("keyup", this.keyUp);
    } else {
      document.removeEventListener("keydown", this.keyDown);
      document.removeEventListener("keyup", this.keyUp);
    }
  }

  keyDown = (e) => {
    if (isKeyWithNote(e.code)) {
      this.props.dispatch(
        SET_ACTIVE_NOTE,
        getNoteByKey(e.code, this.props.octave)
      );
    }
  };

  keyUp = (e) => {
    if (isKeyWithNote(e.code)) {
      this.props.dispatch(
        REMOVE_ACTIVE_NOTE,
        getNoteByKey(e.code, this.props.octave)
      );
    }
  };

  render() {
    return (
      <div className={styles.board}>
        <Keys />
      </div>
    );
  }
}

export default connectStoreon("useKeyboard", "octave", "activeNotes", KeyBoard);
