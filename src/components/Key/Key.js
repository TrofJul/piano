import React, { useRef, useEffect, useCallback, useMemo } from "react";
import styles from "./Key.module.css";
import cn from "classnames";
import { useStoreon } from "storeon/react";
import {
  getKeyLetterFromKey,
  prevent,
  writeNoteForHints,
} from "../../utils/index";
import { REMOVE_ACTIVE_NOTE, SET_ACTIVE_NOTE } from "../../store/events";

const styleKey = styles.key;
const styleActive = styles.active;
const styleSharp = styles.sharp;
const styleContent = styles.content;

const Key = React.memo(function Key(props) {
  const { dispatch, useKeyboard, hintsShown, volume } = useStoreon(
    "useKeyboard",
    "hintsShown",
    "volume"
  );
  const src = useMemo(
    () =>
      process.env.PUBLIC_URL +
      "/audio/" +
      props.note.noteName.toLowerCase() +
      ".ogg",
    [props.note]
  );
  const audio = useRef(null);

  const playAudio = useCallback(() => {
    audio.current.currentTime = 0;
    audio.current.play();
  }, []);

  useEffect(() => {
    if (props.active) {
      playAudio();
    }
  }, [playAudio, props.active]);

  useEffect(() => {
    audio.current.volume = volume;
  }, [volume]);

  const onMouseDown = useCallback(() => {
    dispatch(SET_ACTIVE_NOTE, props.note);
  }, [dispatch, props.note]);

  const onMouseUp = useCallback(() => {
    dispatch(REMOVE_ACTIVE_NOTE, props.note);
  }, [dispatch, props.note]);

  return (
    <div
      className={cn({
        [styleKey]: true,
        [styleSharp]: props.styled,
      })}
      onMouseDown={useKeyboard ? undefined : onMouseDown}
      onMouseUp={useKeyboard ? undefined : onMouseUp}
      onMouseOut={useKeyboard ? undefined : onMouseUp}
      onContextMenu={prevent}
    >
      <div
        className={cn({ [styleContent]: true, [styleActive]: props.active })}
      >
        {useKeyboard && (
          <span className={styles.button}>
            {getKeyLetterFromKey(props.note.keyName)}
          </span>
        )}
        {hintsShown && (
          <span className={styles.hint}>{writeNoteForHints(props.note)}</span>
        )}
        <audio src={src} ref={audio}></audio>
      </div>
    </div>
  );
});

export default Key;
