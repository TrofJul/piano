import React, { useCallback } from "react";
import styles from "./Volume.module.css";
import { useStoreon } from "storeon/react";
import { CHANGE_VOLUME } from "../../store/events";

function Volume() {
  const { dispatch } = useStoreon();

  const changeVolume = useCallback(
    (e) => {
      dispatch(CHANGE_VOLUME, e.target.value / 100);
    },
    [dispatch]
  );

  return (
    <div className={styles.volume}>
      <span className={styles.option}>Громкость:</span>
      <input
        className={styles.range}
        min="0"
        max="100"
        type="range"
        onChange={changeVolume}
      />
    </div>
  );
}

export default Volume;
