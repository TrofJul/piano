import React from "react";
import Key from "../Key/Key";
import { isSharp, getNotes } from "../../utils/index";
import { useStoreon } from "storeon/react";

function Keys() {
  const { useKeyboard, activeNotes, numOfOctaves, octave } = useStoreon(
    "useKeyboard",
    "activeNotes",
    "numOfOctaves",
    "octave"
  );
  const notes = React.useMemo(
    () => getNotes(useKeyboard, numOfOctaves, octave),
    [numOfOctaves, useKeyboard, octave]
  );

  return (
    <>
      {notes.map((note) => {
        return (
          <Key
            styled={isSharp(note)}
            key={note.noteName}
            note={note}
            active={activeNotes.includes(note)}
          />
        );
      })}
    </>
  );
}

export default React.memo(Keys);
