import React, { useState } from "react";
import styles from "./MelodyPlayer.module.css";
import { useStoreon } from "storeon/react";
import { getNoteByKey, delay } from "../../utils/index";
import { MELODY, LOW } from "../../constants/index";
import { REMOVE_ACTIVE_NOTE, SET_ACTIVE_NOTE } from "../../store/events";

function MelodyPlayer() {
  const [isActive, setIsActive] = useState(false);

  const { dispatch } = useStoreon();

  const playMelody = () => {
    let timeFromStart = 0;
    setIsActive(true);
    MELODY.forEach((note) => {
      delay(timeFromStart).then(() => {
        dispatch(SET_ACTIVE_NOTE, getNoteByKey(note.Key, LOW));
        delay(note.time).then(() =>
          dispatch(REMOVE_ACTIVE_NOTE, getNoteByKey(note.Key, LOW))
        );
      });
      timeFromStart += note.time;
    });
    delay(timeFromStart).then(() => {
      setIsActive(false);
    });
  };

  return (
    <div className={styles.item}>
      <span className={styles.option}>Воспроизвести мелодию</span>
      <button
        className={styles.play_button}
        onClick={isActive ? undefined : playMelody}
        disabled={isActive}
      ></button>
    </div>
  );
}

export default React.memo(MelodyPlayer);
