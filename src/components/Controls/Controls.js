import React from "react";
import styles from "./Controls.module.css";
import { connectStoreon } from "storeon/react";
import Volume from "../Volume/Volume";
import ControlItem from "../ControlItem/ControlItem";
import MelodyPlayer from "../MelodyPlayer/MelodyPlayer";
import { LOW, HIGH } from "../../constants/index";
import {
  SHOW_HINTS,
  SWITCH_OCTAVE,
  SWITCH_NUM_OF_OCTAVES,
  SWITCH_CONTROL_KEYBOARD,
} from "../../store/events";
import { thereAreActiveNotes } from "../../utils/index";

class Controls extends React.Component {
  switchControl = (e) => {
    this.props.dispatch(SWITCH_CONTROL_KEYBOARD, e.target.checked);
  };

  showHints = (e) => {
    this.props.dispatch(SHOW_HINTS, e.target.checked);
  };

  switchNumOfOctaves = (e) => {
    this.props.dispatch(SWITCH_NUM_OF_OCTAVES, e.target.checked ? 1 : 2);
  };

  switchOctave = (e) => {
    this.props.dispatch(SWITCH_OCTAVE, e.target.checked ? HIGH : LOW);
  };

  render() {
    return (
      <div className={styles.settings}>
        <div className={styles.row1}>
          <ControlItem
            leftOption="Мышка"
            rightOption="Клавиатура"
            onClick={this.switchControl}
            disabled={thereAreActiveNotes(this.props.activeNotes)}
          />
          <ControlItem title="Включить подсказки" onClick={this.showHints} />
          <Volume />
        </div>
        <div className={styles.row2}>
          <ControlItem
            title="Количество октав: "
            leftOption="2"
            rightOption="1"
            onClick={this.switchNumOfOctaves}
            disabled={thereAreActiveNotes(this.props.activeNotes)}
          />

          {this.props.numOfOctaves === 1 ? (
            <ControlItem
              title="Октава: "
              leftOption="Ниже"
              rightOption="Выше"
              onClick={this.switchOctave}
              disabled={thereAreActiveNotes(this.props.activeNotes)}
            />
          ) : (
            <MelodyPlayer />
          )}
        </div>
      </div>
    );
  }
}

export default connectStoreon("numOfOctaves", "activeNotes", Controls);
